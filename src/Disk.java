
public class Disk {
	String movie;
	
	Disk(String movie){
		this.movie = movie;
	}
	
	public String toString() {
		return movie;
	}
}
