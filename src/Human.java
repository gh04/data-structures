
public class Human {
	String name;

	Human(String name) {
		this.name = name;
	}
	
	public String toString() {
		return name;
	}
}
