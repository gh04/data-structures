import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import java.util.Vector;

public class Programa {

	public static void main(String[] args) {
//		List - структура от данни която съдържа подаден тип обекти
//		ArrayList - маскиран масив, който се увеличава с коефициент
//		1.5 когато се напълни (начален капацитет: 10)
//		LinkedList - всеки елемент има адрес в паметта и препратка към
//		следващия елемент
//		Vector - работи като ArrayList, но с коефициент 2, също така е синхронизиран

//		List приема само обекти затова не може да иползваме примитиви
//		int -> Integer	double -> Double	char -> Character	boolean -> Boolean

		List<Integer> list1 = new LinkedList<Integer>();

		List<Character> list2 = new ArrayList<Character>();

		List<Double> list3 = new Vector<Double>();

		List<Human> list4 = new LinkedList<Human>();

//		list1.add(5);
//		list1.add(7);
//		list1.add(34);
//		System.out.println(list1);

//		list1.set(2, 17);
//		System.out.println(list1.get(2));

//		System.out.println(list1.size());
//
//		list1.remove(1);
//		System.out.println(list1);
//
//		System.out.println(list1.size());

//		list2.add('@');
//		list2.add('#');
//		list2.add('4');
//		System.out.println(list2.contains('4'));

//		System.out.println(list2);
//		list2.clear();
//		System.out.println(list2);

		
		
//		Stack - структура от данни която работи на принципа
//		Последен вътре, Първи вън(Last-In-First-Out)
//		Stack разширява Vector класа затова подобно на List приема обекти

		Stack<Disk> filmi = new Stack<Disk>();

//		filmi.push(new Disk("Star Wars I"));
//		filmi.push(new Disk("Star Wars II"));
//		filmi.push(new Disk("Star Wars III"));
//		
//		System.out.println(filmi.peek());

//		filmi.push(new Disk("Star Wars IV"));
//		filmi.push(new Disk("Star Wars V"));
//		filmi.push(new Disk("Star Wars VI"));
//		
//		System.out.println(filmi.peek());

//		filmi.pop();
//		
//		System.out.println(filmi.peek());

		
		
//		Queue - структура от данни която работи на принципа
//		Първи вътре, Първи вън(First-In-First-Out)
//		Queue имплементира ArrayDeque, LinkedList и PriorityQueue
//		Queue разширява Deque, BlockingQueue и BlockingDeque
//		Queue приема само обекти

		Queue<Human> opashka = new LinkedList<Human>();

//		opashka.add(new Human("John"));
//		opashka.add(new Human("John the II"));
//		opashka.add(new Human("Jeff"));
//		opashka.add(new Human("Mark"));
//		opashka.add(new Human("Maria"));
//		
//		System.out.println(opashka.peek());

//		opashka.poll();
//		
//		System.out.println(opashka.peek());
		
	}
}
